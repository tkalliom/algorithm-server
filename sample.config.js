/*  This is a sample file defining algorithm information.
    Replace the values with the correct ones, and place the config.js in the
    application root.
*/

const algorithm = {
    name: "a_complex_algorithm",
    version: 1,
    // host: complexalgorithm, - analysis service will use registration source if this is not specified
    // port: 3002,
    // remove *dependencies if algorithm does not depend on anything
    current_frame_dependencies: ["a_simple_algorithm:2"] // "version must be N or greater"
};

const program = {
    command: "python", // interpreter or binary
    args: ["echo.py"] //may be left empty
}

const config = {
    algorithm: algorithm,
    program: program
}

module.exports = config;
