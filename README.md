# HTTP server wrapper for 360VI algorithms

This is a simple Node.js HTTP server which handles two tasks:
1. registering an algorithm, and its dependencies, with the [analysis platform]((https://bitbucket.org/tkalliom/360vi-platform)) which provides an analysis API
1. handing over frame analysis requests from the platform to the algorithm implementation (the actual data fows via an exchange directory, residing in RAM)

This wrapper is hopefully useful for most algorithms. However, if you for some reason find it inadequate - for example, if you don't have Node.js in your environment - you can make your own implementation of the registration and request handling.



- - -



## How to integrate

Your algorithm binary or script needs to follow a certain command flow when using this wrapper. Algorithms are run in Docker containers on a single physical server.


### 1. Algorithm implementation requirements

Your algorithm implementation can be a binary or a script. It will be run as a process with a lifecycle that starts as the first frame of a video comes in, and ends when the last frame is processed.

Launching the program should be possible with the exact same arguments for any video.

1. If applicable, set up any internal state you need for the particular video.
1. Read the initialization line from standard input. It is of the format `video_identifier_abc123	/path/to/exhangedirectory/abc123` (tab-separated).
1. Read a line from standard input. It is of the format `field1_name	field1_value	fieldN_name	fieldN_value` (tab-separated, order not guaranteed). The field names are:
    * `filename`: the name of the file containing the pixels of the frame in BGR24. Must be appended to the directory on the initialization line; e.g. `directory ' "/" + filename`
    * `number`: the running number of the frame in the video (zero-based)
    * `pts`: the presentation timestamp of the frame
    * `width`, `height`: the shape of the frame (needed to interpret the datafile correctly)
1. Map the frame datafile into read-only process memory (it is the responsibility of the platform to make sure it is in `tmpfs`, to prevent multiple copy operations)
1. If you have in the registration defined that you need analysis results from other algorithms, you may assume they are now available inside the video directory. The pattern is `124.dat -> 124.required_previous_algorithm.json`
1. Process the frame.
1. Output any results you obtained in the video directory. The file name must be `current_input_frame.youralgorithm.json`, but it may contain results regarding previously processed frames. All JSON files must have an array as their root and even if you do not have any results, you must ouput at least the empty array `[]`. See the analysis platform API documentation for the format of the result objects. Also output to stdout the message `Frame n analyzed`, where n is the zero-indexed number of the frame.
1. If the standard input is still open, go to 3.
1. Output a file named `eof.youralgorithm.json` in the video directory. It must contain either any results left in your buffers or the empty array `[]`. Also output to stdout the message `Video analyzed`.
1. Cleanup any internal state for the video, if applicable.

Make sure that the lines you print to standard output are also flushed immediately, not left in a buffer. You should first output the JSON files, then the stdout lines.

The algorithms only read from and write to the video directory, they do not modify or delete.


### 2. Wrapper configuration

This repository contains a `sample.config.json`. Make a copy of it in your program root as `config.json`, with the correct values. The `algorithm` part affects communication with the platform, and the `program` part defines how to launch the algorithm implementation.

#### Names and dependencies

The algorithm must have a unique name, consisting of alphanumeric characters, underscores and dashes. It has an integer version, which allows to determine which iteration of an algorithm the an analysis result was obtained which, and to indicate dependency compatibility.

An algorithm may depend on others in the following ways (this list is subject to refinement):
- `current_frame`: when algorithm B is analyzing frame 5 of 10, it must have the results A gave for frame 5
- `whole_video`: when algorithm B is analyzing frame 5 of 10, it must have the results A gave for frames 0-10

**Note**: if algorithm A has a buffer, it might output nothing after processing frame 5, and then output results for frame 5 after processing frame 8. In this case, B would not have the results of A available when processing 5, unless the `whole_video` dependency is used. A more elegant solution to this situation would be desirable.

#### Communication with the platform.

As long as you are only building Docker images, you should not need to touch the `host` and `port` configurations, as Docker uses an internal network where each container gets its own hostname. These configuration options are mainly useful for testing the platform outside of Docker - when run on a single computer, each algorithm needs to be using a different port.

#### Algorithm program launching

Give the command and any necessary arguments to run the algorithm implementation. The binary or script should be located in the program root.


### 3. Building a Docker container for the algorithm

The Docker [Get Started](https://docs.docker.com/get-started/) guide is recommended reading.

See [sample-algorithm-a](https://bitbucket.org/tkalliom/sample-algorithm-a) and [sample-algorithm-b](https://bitbucket.org/tkalliom/sample-algorithm-a) for examples of algorithms with Dockerfiles.

1. Place the `app.js` file inside a directory named `algorithm-server` in your program. Linking this repository as a Git submodule in your repository is a handy way of achieving this without placing external code in your own repository.
1. Make a `config.js` in the program root as described in the previous section.
1. Make a Dockerfile specifying the base image and setting up the runtime environment for your application. If you do not have to compile anything, this should be easy. However, it is possible specify compiling both external dependencies and your own program in the Dockerfile. [Multi-stage builds](https://docs.docker.com/engine/userguide/eng-image/multistage-build/#before-multi-stage-builds) are recommended if you need to compile.
1. Run `docker build . --tag your_dockerhub_username/algorithmname`

You can optionally publish the image in Docker hub (one private image per account is possible), which makes it easier to deploy your algorithm in the platform. Another possibility is for the platform maintainer to build the image from source.

An individual container can be started with `docker run -d --name=a_name_for_the_container --network=network-shared-with-platform -v /tmp/360vi-analysis:/tmp/360vi-analysis your_dockerhub_username/algorithmname`.