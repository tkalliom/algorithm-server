const child_process = require("child_process");
const readline = require("readline");
const http = require("http");
const querystring = require("querystring");
const url = require("url");
const debug = require("util").debuglog("algorithm-server");

const config = require("../config");

const handlers = {};
const make_worker = () => child_process.spawn(config.program.command, config.program.args);
var pooled_worker = make_worker();

const server = http.createServer(handleRequest);
server.listen(process.env.PORT || config.algorithm.port || 3001 , err => {
    if (err) {
        throw err;
    }
    console.log(`Listening on port ${process.env.PORT || config.algorithm.port || 3001}, algorithm ${config.algorithm.name}`);

    registerAlgorithm();
});

function registerAlgorithm() {
    const reqBody = querystring.stringify(config.algorithm);
    const reqOptions = {
        host: process.env.REGISTRATION_HOST || "analysis-service",
        path: process.env.REGISTRATION_PATH || "/algorithms",
        port: process.env.REGISTRATION_PORT || 80,
        method: "POST",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(reqBody)
        }
    }
    const req = http.request(reqOptions, res => {
        if (res.statusCode != 200 && res.statusCode != 201 && res.statusCode != 204 && res.statusCode != 409) {
            throw `Received a ${res.statusCode} response from registration server`;
        }
        console.log(`Registered algorithm ${config.algorithm.name} with ${reqOptions.host}`)
    })
    .on("error", e => {console.error(e)});
    req.end(reqBody);
}

function handleRequest(request, response) {
    if (url.parse(request.url).pathname != "/analyze") {
        return respond({request: request, response: response}, 404);
    } else if (request.method != "POST") {
        return respond({request: request, response: response}, 405);
    } else if (request.headers["content-type"] != "application/x-www-form-urlencoded") {
        return respond({request: request, response: response}, 415);
    }

    var data = '';
    request.on("data", chunk => data += chunk)
    .on("error", err => {
        console.error(err);
        return respond({request: request, response: response}, 500);
    })
    .on("end", () => {
        var post = querystring.parse(data);

        if (!("video_id" in post) || !post["video_id"] || !("video_directory" in post) || !(post["video_directory"])) {
            return respond({request: request, response: response}, 422);
        }

        if ("video_eof" in post && post["video_eof"] == "true") {
            if (post.video_id in handlers) {
                handlers[post.video_id].requests.push({request: request, response: response});
                handlers[post.video_id].worker.stdin.end();
                delete handlers[post.video_id].worker;
            } else {
                return respond({request: request, response: response}, 409);
            }
        } else {
            const numbers = ["frame_number", "frame_pts", "frame_width", "frame_height"];
            const strings = ["frame_filename"];
            if (strings.some(string => !(string in post) || !post[string]) || numbers.some(number => !(number in post) || isNaN(post[number]))) {
                return respond({request: request, response: response}, 422);
            }

            if (post.video_id in handlers && !("worker" in handlers[post.video_id])) {
                delete handlers[post.video_id];
            }

            const worker_exists = post.video_id in handlers;
            const handler = worker_exists ? handlers[post.video_id] : {worker: pooled_worker, requests: [{request: request, response: response}]};

            if (!worker_exists) {
                debug(`Encountered new video id ${post.video_id}, will pop worker and make a new one`);
                pooled_worker = make_worker();

                handlers[post.video_id] = handler;
                handler.worker.stdin.write(post.video_id + "\t" + post.video_directory + "\n");
                const outLines = readline.createInterface({input: handler.worker.stdout});
                outLines.on("line", line => {
                    if (line.search(/(?:Frame|Video) (?:([0-9]+) )?analyzed*/) > -1) {
                        return respond(handler.requests.shift(), 200, line);
                    }
                })
                .on("error", err => {
                    console.error(err);
                    return respond(handler.requests.shift(), 500);
                });
                const errLines = readline.createInterface({input: handler.worker.stderr});
                errLines.on("line", line => {
                    console.error(line); // This might be e.g. a deprecation warning
                })
                .on("err", err => {
                    console.error(err);
                    return respond(handler.requests.shift(), 500)
                })
            } else {
                handler.requests.push({request: request, response: response});
            }

            const frameline = ["number", post.frame_number, "pts", post.frame_pts, "filename", post.frame_filename, "width", post.frame_width, "height", post.frame_height].join("\t");

            debug(`Sending worker a command: ${frameline}`);
            handler.worker.stdin.write(frameline + "\n");
        }
    })
}

function respond(reqRes, statusCode, body) {
    const req = reqRes.request;
    const res = reqRes.response;

    console.log(`${statusCode} - ${req.method} ${url.parse(req.url).pathname}`);
    res.statusCode = statusCode;
    if (body) {
        res.end(body);
    } else {
        res.end();
    }
}
